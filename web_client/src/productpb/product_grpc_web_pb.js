/**
 * @fileoverview gRPC-Web generated client stub for grpc.application.api.grpc
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.grpc = {};
proto.grpc.application = {};
proto.grpc.application.api = {};
proto.grpc.application.api.grpc = require('./product_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.grpc.application.api.grpc.ProductServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.grpc.application.api.grpc.ProductServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.grpc.application.api.grpc.Product,
 *   !proto.grpc.application.api.grpc.ProductResult>}
 */
const methodDescriptor_ProductService_CreateProduct = new grpc.web.MethodDescriptor(
  '/grpc.application.api.grpc.ProductService/CreateProduct',
  grpc.web.MethodType.UNARY,
  proto.grpc.application.api.grpc.Product,
  proto.grpc.application.api.grpc.ProductResult,
  /**
   * @param {!proto.grpc.application.api.grpc.Product} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.grpc.application.api.grpc.ProductResult.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.grpc.application.api.grpc.Product,
 *   !proto.grpc.application.api.grpc.ProductResult>}
 */
const methodInfo_ProductService_CreateProduct = new grpc.web.AbstractClientBase.MethodInfo(
  proto.grpc.application.api.grpc.ProductResult,
  /**
   * @param {!proto.grpc.application.api.grpc.Product} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.grpc.application.api.grpc.ProductResult.deserializeBinary
);


/**
 * @param {!proto.grpc.application.api.grpc.Product} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.grpc.application.api.grpc.ProductResult)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.grpc.application.api.grpc.ProductResult>|undefined}
 *     The XHR Node Readable Stream
 */
proto.grpc.application.api.grpc.ProductServiceClient.prototype.createProduct =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/grpc.application.api.grpc.ProductService/CreateProduct',
      request,
      metadata || {},
      methodDescriptor_ProductService_CreateProduct,
      callback);
};


/**
 * @param {!proto.grpc.application.api.grpc.Product} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.grpc.application.api.grpc.ProductResult>}
 *     Promise that resolves to the response
 */
proto.grpc.application.api.grpc.ProductServicePromiseClient.prototype.createProduct =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/grpc.application.api.grpc.ProductService/CreateProduct',
      request,
      metadata || {},
      methodDescriptor_ProductService_CreateProduct);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.grpc.application.api.grpc.Empty,
 *   !proto.grpc.application.api.grpc.ProductResult>}
 */
const methodDescriptor_ProductService_List = new grpc.web.MethodDescriptor(
  '/grpc.application.api.grpc.ProductService/List',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.grpc.application.api.grpc.Empty,
  proto.grpc.application.api.grpc.ProductResult,
  /**
   * @param {!proto.grpc.application.api.grpc.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.grpc.application.api.grpc.ProductResult.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.grpc.application.api.grpc.Empty,
 *   !proto.grpc.application.api.grpc.ProductResult>}
 */
const methodInfo_ProductService_List = new grpc.web.AbstractClientBase.MethodInfo(
  proto.grpc.application.api.grpc.ProductResult,
  /**
   * @param {!proto.grpc.application.api.grpc.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.grpc.application.api.grpc.ProductResult.deserializeBinary
);


/**
 * @param {!proto.grpc.application.api.grpc.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.grpc.application.api.grpc.ProductResult>}
 *     The XHR Node Readable Stream
 */
proto.grpc.application.api.grpc.ProductServiceClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/grpc.application.api.grpc.ProductService/List',
      request,
      metadata || {},
      methodDescriptor_ProductService_List);
};


/**
 * @param {!proto.grpc.application.api.grpc.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.grpc.application.api.grpc.ProductResult>}
 *     The XHR Node Readable Stream
 */
proto.grpc.application.api.grpc.ProductServicePromiseClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/grpc.application.api.grpc.ProductService/List',
      request,
      metadata || {},
      methodDescriptor_ProductService_List);
};


module.exports = proto.grpc.application.api.grpc;

