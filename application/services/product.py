import time
from util import context as ctx
from model.product import Product
from protobuffers import product_pb2_grpc, product_pb2


class ProductServiceServicer(product_pb2_grpc.ProductServiceServicer):

    def CreateProduct(self, request, context):
        x = "creating"
        print (x)
        new_product = Product(request.name)
        ctx.ProductList.append(new_product)
        result = product_pb2.ProductResult()
        result.name = new_product.Name
        result.id = new_product.ID
        print (result)
        return result

    def List(self, request, context):
        x = "List"
        print (x)
        for product in ctx.ProductList:
            time.sleep(2)
            result = product_pb2.ProductResult()
            result.name = product.Name
            result.id = product.ID
            yield result


