import uuid


class Product:
	ID = ""
	Name = ""

	def __init__(self, name):
		self.ID = uuid.uuid4().__str__()
		self.Name = name

	def __str__(self):
		return f"ID: {self.ID} , Name: {self.Name}"

