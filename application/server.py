import contextvars
import os
import time
import logging
import grpc

from util import context as ctx
from concurrent import futures
from grpc_reflection.v1alpha import reflection

from protobuffers import (product_pb2_grpc)
from protobuffers import product_pb2
from services.product import ProductServiceServicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
logging.basicConfig(level=logging.INFO)

def serve():
    ctx.ProductList = list()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    SERVICE_NAMES = (
        product_pb2.DESCRIPTOR.services_by_name['ProductService'].full_name,
        reflection.SERVICE_NAME
    )
    reflection.enable_server_reflection(SERVICE_NAMES, server)

    # add services
    product_pb2_grpc.add_ProductServiceServicer_to_server(
        ProductServiceServicer(), server)

    # start server
    address = '%s:%s' % (os.environ['HOST'], os.environ['PORT'])
    logging.info('Starting grpc server at %s', address)

    server.add_insecure_port(address)

    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()

